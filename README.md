## Available Scripts
First of all you need to clone the repository:
## `git clone git@bitbucket.org:maamunrcd/roulette-game.git`
Install node packages 
## `npm install`
Run the project
## `npm start`
Runs the app in the development mode.
## `Open your browser and browse http://localhost:3000`
The page will reload if you make edits.<br />
You will also see any lint errors in the console.
## `yarn build`
Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.