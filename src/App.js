import React from 'react';
import './App.css';
import StatsView from './components/StatsView';

const App = () => {
  return (
    <div className="App">
      <div className="container">
        <div className="row">
          <div className="col-12">
          <StatsView/>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
