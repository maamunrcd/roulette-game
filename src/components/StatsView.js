import React, { useState, useEffect } from "react";
import loadingImage from "../loading.gif";
const baseUrl = "http://dev-games-backend.advbet.com/v1/ab-roulette/1/";
const axios = require("axios");
const StatsView = () => {
  const [stats, setStats] = useState([]);
  const [board, setBoard] = useState([]);
  const [nextGameItem, setNextGameItem] = useState([]);
  const [error, setError] = useState(false);
  const [gameResultId, setGameResultId] = useState(false);
  const [gameMessageHistory, setGameMessageHistory] = useState([]);
  const [gameMessage, setGameMessage] = useState("");
  const [loading, setLoading] = useState(false);
  const getStatsView = async () => {
    try {
      const response = await axios.get(`${baseUrl}stats?limit=200`);
      setStats(response.data);
    } catch (error) {
      setError(error);
    }
  };
  const nextGame = async () => {
    try {
      const response = await axios.get(`${baseUrl}nextGame`);
      setNextGameItem(response.data);
    } catch (error) {
      setError(error);
    }
  };
  const gameResult = async gameId => {
    setLoading(true);
    try {
      const response = await axios.get(`${baseUrl}game/${gameId}`);
      setGameResultId(response.data);
      if (response.data.result) {
        setLoading(false);
        const messageHistory = [...gameMessageHistory];
        messageHistory.push(
          `Game ${nextGameItem.id} ended, result is ${response.data.result}`
        );
        setGameMessageHistory(messageHistory);
      }
    } catch (error) {
      setLoading(false);
      setError(error);
    }
  };

  const gameBoard = async () => {
    try {
      const response = await axios.get(`${baseUrl}configuration`);
      setBoard(response.data);
    } catch (error) {
      setError(error);
    }
  };
  useEffect(() => {
    getStatsView();
    gameBoard();
    nextGame();
  }, []);
  useEffect(() => {
    gameBoard();
  }, [gameResultId]);
  useEffect(() => {
    const gameTimer = setInterval(() => {
      nextGameItem.fakeStartDelta -= 1;
      if (nextGameItem.fakeStartDelta > 0) {
        setGameMessage(
          `Game ${nextGameItem.id} will start in ${nextGameItem.fakeStartDelta} sec`
        );
        return;
      }
      
      gameResult(nextGameItem.id);
      getStatsView();
      gameBoard();
      nextGame();
      return;
    }, 1000);
    return () => clearInterval(gameTimer);
  }, [nextGameItem.fakeStartDelta, gameResult, nextGameItem.id]);
  return (
    <div className="statsView">
      {error && (<div class="alert alert-danger" role="alert">
        Something is wrong!
      </div>)}
      <table id="stats" className="table">
        <tbody>
          <tr>
            <td>&nbsp;</td>
            <th colSpan={5} className="cold">
              Cold
            </th>
            <th colSpan={27} className="neutral">
              Neutral
            </th>
            <th colSpan={5} className="hot">
              Hot
            </th>
          </tr>
          <tr>
            <th>Slot</th>
            {stats &&
              stats.map((stat, index) => (
                <td
                  key={`stat_${index}`}
                  className={`btn-${board &&
                    board.colors &&
                    board.colors[stat.result]}`}
                >
                  {stat.result}
                </td>
              ))}
          </tr>
          <tr>
            <th>Hits</th>
            {stats &&
              stats.map((stat, index, stateItem) =>
                index < 5 ? (
                  <td key={`stat_${index}`} className="cold">
                    {stat.count}
                  </td>
                ) : index > stateItem.length - 6 ? (
                  <td key={`stat_${index}`} className="hot">
                    {stat.count}
                  </td>
                ) : (
                  <td key={`stat_${index}`}>{stat.count}</td>
                )
              )}
          </tr>
        </tbody>
      </table>
      <div className="boardView">
        <div className="row">
          <div className="col-12">
            <h3 className="title text-left">Game Board</h3>
          </div>
        </div>
        <div className="row">
          <div className="col-6">
            <div className="boardItems d-flex justify-content-between flex-wrap">
              {board &&
                board.results &&
                board.colors &&
                board.results.map((result, index) => (
                  <button key={`key_${index}`} className={`btn-${board.colors[result]} ${gameResultId && gameResultId.result && gameResultId.outcome === result ? 'result-element': ''}`}>
                    {result}
                  </button>
                ))}
            </div>
          </div>
          <div className="col-1">
            <div className="d-flex d-flex align-items-center loading-wrapper">
              {loading && (
                <img className="img-fluid" src={loadingImage} alt="loading" />
              )}
            </div>
          </div>
          <div className="col-5">
            <div className="historyBoard text-left">
              {loading ? (
                <div className="cown-down">Spinning the wheel</div>
              ) : (
                <div className="cown-down">{gameMessage && gameMessage}</div>
              )}

              {gameMessageHistory && (
                <div className="history-list">
                  {gameMessageHistory.map(message => (
                    <p>{message}</p>
                  ))}
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default StatsView;
